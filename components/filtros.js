import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, {Component} from 'react';
import axios from 'axios';

const url = 'https://elclubgroup.co/api/';

class Filtros extends Component {

  state={
    data:[],
    marcas:[]
  }
  
  peticionGet=()=>{
    axios.get(url+'categorias').then(response =>{
      console.log(response.data);
      this.setState({data: response.data});
    }) 
    axios.get(url+'marcas').then(response =>{
      console.log(response.data);
      this.setState({marcas: response.data});
    })     
  }

  componentDidMount(){
    this.peticionGet();
  }

  render(){
    return (
      <div>
        <div className="bloque1 fd-white">
          <h2>Aniam Store</h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
          <button type="button" name="button"><FontAwesomeIcon icon={['fab', 'instagram']} /> Instagram</button>
          <button type="button" name="button"><FontAwesomeIcon icon={['fab', 'whatsapp']} /> WhatsApp</button>
        </div>
        <div className="bloque2 fd-white">
          <div className="filtros">
            <h2>Filtros</h2>
            <button className="btn-limpiar" type="button" name="button">Limpiar</button>
          </div>
          <div className="Precio">
            <h2>Precio</h2>
            <input type="range" className="slider" id="customRange1" min="0" max="50"/>
            Precio: $<span>6.900</span> COP - $<span>206.640</span> COP
          </div>
          <div className="Marcas">
            <h2>Marcas</h2>
            {this.state.marcas.map(function(datos, index){
              return(
                <div className="form-check" key={index}>
                  <input className="form-check-input" type="checkbox" value={datos.id} id={'check_marca'+datos.id} />
                  <label className="form-check-label" >
                    {datos.Nombre}
                  </label>
                </div>
                )
            }, this)}
          </div>
          <div className="Categorias">
            <h2>Categorías</h2>
           {this.state.data.map(function(datos, index){
              return(
                <div className="form-check" key={index}>
                  <input className="form-check-input" type="checkbox" value={datos.id} id={'check_categoria'+datos.id} />
                  <label className="form-check-label" >
                    {datos.Nombre}
                  </label>
                </div>
                )
            }, this)}
            
          </div>
        </div>
        <div className="sesion-responsive">
          <div className="row filtros-responsive">
            <div className="col-6 activo">Categorías</div>
            <div className="col-6">Marcas</div>
          </div>
          <div className="bloque-responsive fd-white">
            <div className="item">
              <img src="/images/ct1.png" />
              Cuidado Facial
            </div>
            <div className="item">
              <img src="/images/ct3.png" />
              Cuidado Facial
            </div>
            <div className="item">
              <img src="/images/ct2.png" />
              Belleza
            </div>
            <div className="item">
              <img src="/images/ct4.png" />
              Cremas
            </div>
            <div className="item">
              <img src="/images/ct1.png" />
              Cabello
            </div>
            <div className="item">
              <img src="/images/ct3.png" />
              Cuidado Facial
            </div>
            <div className="item">
              <img src="/images/ct2.png" />
              Belleza
            </div>
            <div className="item">
              <img src="/images/ct4.png" />
              Cremas
            </div>
          </div>
          <img src="/images/banner-responsive.svg" className="w-100"/>
        </div>
        
      </div>
    )
  }
  
}

export default Filtros
