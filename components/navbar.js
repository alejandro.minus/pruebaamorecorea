import 'bootstrap/dist/css/bootstrap.css'

const NavBar = () => {
  return (
    <nav className="navbar navbar-expand-lg navbar-light">
      <div className="container-fluid">
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <a className="navbar-brand" href="#">
        <div className="titulo">Aniam Store</div>
        <div className="subtitulo">by amore<span>corea</span></div>
        </a>
        <div className="collapse navbar-collapse" id="navbarTogglerDemo03">
          <form className="d-flex m-auto search">
            <input className="form-control" type="search" placeholder="Tu tocador necesita esto..." aria-label="Search" />
            <button className="btn btn-outline-success" type="submit">Buscar</button>
          </form>
          <ul className="navbar-nav m-left mb-2 mb-lg-0">
            <li className="nav-item">              
              <a className="nav-link btn-tienda" aria-current="page" href="#">
                <img src="/images/shop.svg" /> Tienda
              </a>
            </li>
            <li className="nav-item">              
              <a className="nav-link btn-carrito" href="#">
                <img src="/images/car.svg"/> Carrito
                <span>1</span>
              </a>
            </li>
            <li className="nav-item">              
              <a className="nav-link btn-comentarios" href="#" tabIndex="-1">
                <img src="/images/comentario.svg"/>
              </a>
            </li>
          </ul>          
        </div>
      </div>
    </nav>
  )
}

export default NavBar
