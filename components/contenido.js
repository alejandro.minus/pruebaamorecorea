import React, {Component} from 'react';
import axios from 'axios';

const url = 'https://elclubgroup.co/api/';

class Contenido extends Component {

  state={
    data:[]
  }
  
  peticionGet=()=>{
     axios.get(url+'productos').then(response =>{
      console.log(response.data);
      this.setState({data: response.data});
    })
  }

  componentDidMount(){
    this.peticionGet();
  }
   

  render(){
    return(
      <div className="tienda">
        <div className="row">
        
        {this.state.data.map(function(item,index) {
          return(
            <div className="col-lg-3 col-12" key={index}>
              <div className="producto">
                <div className="titulo-marca">
                  <img src="/images/marca.svg"/> {item.NombreMarca}
                </div>
                <img src={'/images/'+item.Imagen} className="d-block w-100" />
                <div className="nombre-shop">{item.Nombre}</div>
                <div className="precio-shop">$19.900 COP</div>
                <div className="tags">
                  <span>velo</span>
                  <span>ansiedad</span>
                  <span>rejuvenece</span>
                  <span>hidrogel</span>
                  <span>aclarante</span>
                </div>
                <button>Agregar al carrito</button>
              </div>
              
            </div>
            )
        },this)}          
            
      </div>
      </div>
      
    );
  }
}

export default Contenido
