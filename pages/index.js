import Head from 'next/head'
import NavBar from '../components/navbar'
import Filtros from '../components/filtros'
import Contenido from '../components/contenido'


import React from 'react'


const App=()=>(

    <div className="container-fluid p-0">
      <Head>
        <title>Amorecorea</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div>
      <NavBar />
      </div>
      <main>

        <img src="/images/banner.svg" className="w-100 banner" />

        <div className="row w-100 fd-gray sesionppal">
          <div className="col-lg-3 col-12 lateral">
          <Filtros />
          </div>
          <div className="col-lg-9 col-12 productos-list">
          <Contenido />
          </div>
        </div>
      </main>     
    </div>

)


export default App
